#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <linux/fb.h>
#include <assert.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

static struct fb_var_screeninfo vi;

static void print_fb_var_screeninfo()
{
    printf("vi.xres: %d\n", vi.xres);
    printf("vi.yres: %d\n", vi.yres);
    printf("vi.xres_virtual: %d\n", vi.xres_virtual);
    printf("vi.yres_virtual: %d\n", vi.yres_virtual);
    printf("vi.xoffset: %d\n", vi.xoffset);
    printf("vi.yoffset: %d\n", vi.yoffset);
    printf("vi.bits_per_pixel: %d\n", vi.bits_per_pixel);
    printf("vi.grayscale: %d\n", vi.grayscale);
    printf("vi.activate: %d\n", vi.activate);
}

int main(int argc, char *argv[])
{
    int fd = open("/dev/fb0", O_RDWR);

    if (ioctl(fd, FBIOGET_VSCREENINFO, &vi) < 0) {
        printf("failed to get fb0 var info\n");
        close(fd);
        return -1;
    }

    print_fb_var_screeninfo();

    vi.activate = FB_ACTIVATE_NOW | FB_ACTIVATE_FORCE;

    if (ioctl(fd, FBIOPUT_VSCREENINFO, &vi) < 0) {
        printf("failed to put fb0 var info\n");
        close(fd);
        return -1;
    }

    close(fd);
    return 0;
}
